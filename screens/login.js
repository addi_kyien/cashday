import React ,{Component} from 'react'
import { View, Text, TextInput, Image,TouchableOpacity,StyleSheet,Button} from 'react-native'
import PhoneInput from 'react-native-phone-input'
import CountryPicker from 'react-native-country-picker-modal'
import { blue } from 'ansi-colors';
export default class Login extends Component{
    static navigationOptions={
        header:null
    }
    constructor() {
        super()
    
        this.onPressFlag = this.onPressFlag.bind(this);
        this.selectCountry = this.selectCountry.bind(this);
        this.state = {
          cca2: 'KE',
    }
}
componentDidMount() {
    this.setState({
      pickerData: this.phone.getPickerData(),
    })
  }

  onPressFlag() {
    this.countryPicker.openModal()
  }

  selectCountry(country) {
    this.phone.selectCountry(country.cca2.toLowerCase())
    this.setState({ cca2: country.cca2 })
  }


render(){
    return(
    <View style={styles.container}>
    <View style={styles.image}>
        <Image
style={{flex:1, height: undefined, width: undefined}}      
 source={require('../images/Cashday2.png')}
       resizeMode="contain"
   />
   </View>
        <View style={styles.loginContainer}>
        <PhoneInput 
        ref={(ref) => {
            this.phone = ref;
          }}
    onPressFlag={this.onPressFlag}
    textStyle={{fontSize:15,
        height: 30,
        backgroundColor: 'rgba(225,225,225,0.2)',
   
    color: '#fff'}}
    textProps={{placeholder:'mobile number',
    placeholderTextColor:'#fff',
    // placeholderTextColor:'rgba(225,225,225,0.7)'
        }}
    initialCountry='ke'

        />
         <CountryPicker
          ref={(ref) => {
            this.countryPicker = ref;
          }}
          onChange={value => this.selectCountry(value)}
          
          translation="eng"
          cca2={this.state.cca2}
              >
        <View></View>
        </CountryPicker>
        
        </View>
    <View style={styles.formcontainer}>   
        
    <TouchableOpacity style={styles.buttonContainer} onPress={()=>this.props.navigation.navigate('home')}>      
      <Text  style={styles.buttonText}>LOGIN WITH MOBILE NUMBER</Text>
</TouchableOpacity>
 
  </View>
<Text style={ { fontSize: 15,color:'#27ae60'} }>
  Don't have an account?<Text onPress={()=>this.props.navigation.navigate('register')
} style={{fontSize:15,color:'#e74c3c',textDecorationLine:'underline'}}>create account</Text>
</Text>
  
    </View>
          
    )
}


}

const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#2c3e50',
        // justifyContent:'center',
        // alignItems: 'center',
        padding:50
    
    },
    image:{
        justifyContent:'center',
        alignContent: 'center',
        flexGrow:0.5,

        

    },
    input:{
        height: 40,
        backgroundColor: 'rgba(225,225,225,0.2)',
        marginBottom: 10,
        padding: 10,
        color: '#fff'
       },
       formcontainer:{
        alignItems: 'center',
        flexGrow: 0.3,
        paddingBottom: 70,
        paddingTop: 10,
        justifyContent: 'flex-start',
       },
       buttonContainer:{
        backgroundColor: '#2980b6',
        paddingVertical: 10,
        padding:30,
        // paddingTop:20
        // flexGrow:0.4
       },
       loginContainer:{
        alignItems: 'center',

        // paddingVertical:30,
        // paddingTop:20,
        height:10,
        justifyContent: 'flex-end',

    },
    logo: {
        position: 'absolute',
        width: 300,
        height: 100
    },
       buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
       }
    
})
