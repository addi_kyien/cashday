import React ,{Component} from 'react'
import { 
    View, Text, TextInput,Picker,
     Image,ScrollView,TouchableOpacity,StyleSheet,Slider} 
from 'react-native'
export default class Register extends Component{
    static navigationOptions={
        header:null
    }
    constructor(props){
        super(props)
        this.state = {
            distance: 10,
            minvalue: 0,
            maxvalue: 100,
            employee_status:null
        }
    }
    render(){
        return(

            <View style={styles.container}>
                <View style={styles.image}>

                            <Image
style={{flex:1, height: undefined, width: undefined}}      
                source={require('../images/Cashday2.png')}
                    resizeMode="contain"
                /></View>
                <ScrollView style={{flex:1}}>

                  <Text  style={styles.inputlabel}>Name</Text>

                 <TextInput style = {styles.input} 
        autoCapitalize="none" 
        onSubmitEditing={() => this.passwordInput.focus()} 
        autoCorrect={false} 
        keyboardType='email-address' 
        returnKeyType="next" 
        placeholder='username' 
        placeholderTextColor='rgba(225,225,225,0.7)'/>
                  <Text  style={styles.inputlabel}>National ID</Text>

  <TextInput style = {styles.input}   
       returnKeyType="go" 
       placeholder='national id' 
       placeholderTextColor='rgba(225,225,225,0.7)' 
       />
                  <Text  style={styles.inputlabel}>Date of Birth</Text>

  <TextInput style = {styles.input}   
       returnKeyType="go" 
       placeholder='Birth date' 
       placeholderTextColor='rgba(225,225,225,0.7)' 
       />
                  <Text  style={styles.inputlabel}>Address</Text>

  <TextInput style = {styles.input}   
       returnKeyType="go" 
       placeholder='user address'
       keyboardType='numeric'
       placeholderTextColor='rgba(225,225,225,0.7)' 
       />
                  <Text  style={styles.inputlabel}>Pincode</Text>

  <TextInput style = {styles.input}   
       returnKeyType="go" 
       ref={(input)=> this.passwordInput = input} 
       placeholder='user pincode' 
       keyboardType='numeric'
       placeholderTextColor='rgba(225,225,225,0.7)' 
       secureTextEntry/>
          <Text  style={styles.inputlabel}>Income Level</Text>
        <View style={{ flexDirection: 'column', marginTop: 10,padding:10 }}>
         <Slider
         style={{ width: 270}}
         step={10}
         minimumValue={this.state.minvalue}
         maximumValue={this.state.maxvalue}
         value={this.state.distance}
         onValueChange={val => this.setState({ distance: val })}
         thumbTintColor='rgb(252, 228, 149)'
         maximumTrackTintColor='#d3d3d3' 
         minimumTrackTintColor='rgb(252, 228, 149)'
            />
             <View style={styles.textCon}>
                    <Text style={styles.colorGrey}>{this.state.minvalue}K</Text>
                    <Text style={styles.colorYellow}>
                        {this.state.distance + 'K'}
                    </Text>
                    <Text style={styles.colorGrey}>{this.state.maxvalue} K</Text>
                </View>
        </View>
        <Text  style={styles.inputlabel}>Employment Status</Text>

            <Picker
            selectedValue={this.state.employee_status}
            style={{ height: 50, width:150,color:'#fff', }}
            onValueChange={(itemValue, itemIndex) => this.setState({employee_status: itemValue})}>
            <Picker.Item label="employed" value="employed" />
            <Picker.Item label="self-employed" value="self-employed" />
            </Picker>
            <Text  style={styles.inputlabel}>Company/Institution</Text>
            <TextInput style = {styles.input}   
                returnKeyType="go" 
                ref={(input)=> this.passwordInput = input} 
                placeholder='company' 
                keyboardType='email-address'
                placeholderTextColor='rgba(225,225,225,0.7)' 
                />
            <Text  style={styles.inputlabel}>Enter 4 Digit Code </Text>

         <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
         {/* <View style={{alignItems:'center'
         ,zIndex:2}}> */}


            <TextInput style = {styles.digitinput}   
                returnKeyType="go" 
                // placeholder='company' 
                keyboardType='numeric'
                />
{/*            
         </View> */}
                <View style={{
                    flex: 2,
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    zIndex:1,
                    position:'absolute',
                    paddingRight:40,
                    
                    }}>
                    
                                <View style={styles.square} />
                                <View style={styles.square} />
                                <View style={styles.square} />
                                <View style={styles.square} />
                    
                </View>
                  </View>
                  <Text  style={styles.inputlabel}>Confirm 4 Digit Code </Text>

<View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
{/* <View style={{alignItems:'center'
,zIndex:2}}> */}


   <TextInput style = {styles.digitinput}   
       returnKeyType="go" 
       // placeholder='company' 
       keyboardType='numeric'
       />
{/*            
</View> */}
       <View style={{
           flex: 2,
           flexDirection: 'row',
           justifyContent: 'space-around',
           zIndex:1,
           position:'absolute',
           paddingRight:40,
           
           }}>
           
                       <View style={styles.square} />
                       <View style={styles.square} />
                       <View style={styles.square} />
                       <View style={styles.square} />
           
       </View>
         </View>
         <TouchableOpacity style={styles.buttonContainer} onPress={()=>this.props.navigation.navigate('verify')}>      
      <Text  style={styles.buttonText}>REGISTER</Text>
</TouchableOpacity>
                </ScrollView>
            </View>
        )
    }

}
const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#2c3e50',
        padding: 30,
        
    
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
       },
    textCon: {
        width: 300,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    colorGrey: {
        color: '#d3d3d3'
    },
    colorYellow: {
        color: 'rgb(252, 228, 149)'
    },

    square: {
        width: 40,
        height: 40,
        marginLeft: 30,
        backgroundColor: '#2c3e50',
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#7f8c8d'
    },
    image:{
        justifyContent:'center',
        alignContent: 'center',
        flexGrow:0.5,
        // paddingBottom: 70

    },
    input:{
        height: 40,
        backgroundColor: 'rgba(225,225,225,0.2)',
        marginBottom: 10,
        padding: 10,
        color: '#fff'
       },
       digitinput:{
        // position:'relative',
        zIndex:2,
        letterSpacing:55,
        marginBottom: 10,
         paddingRight: 20,
        color: '#fff'
       },
       inputlabel:{
           color:'#27ae60',
            fontSize:15
       }
       ,
       buttonContainer:{
        backgroundColor: '#2980b6',
        paddingVertical: 15
       },
       loginContainer:{
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    logo: {
        position: 'absolute',
        width: 300,
        height: 100
    },
       buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
       }
    
})