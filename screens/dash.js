import React,{Component } from "react"
import { View,Text,TextInput,TouchableOpacity,StyleSheet,Dimensions} from "react-native"


import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
export default class Dash extends Component{


    static navigationOptions={
        header:null
    }

    render(){
        return(
            <View style={styles.container}>
               <View style={styles.top}></View>
                    <View style={styles.profile1}>
                    
                    <View style={styles.profile2}>
                    <View style={styles.profile3}>
                    
                    
                        </View>
                    
                    </View>
                    
                    </View>
                    <View style={styles.profile4}></View>
                    <View style={styles.innercontainer}>
                   <View style={styles.box1}>
                   <Text style={styles.text_value}>Your Loan is due in 6 days</Text>
                   </View>
                   <View style={styles.box1}>
                   <TouchableOpacity style={styles.buttonContainer} >      
      <Text  style={styles.buttonText}>REPAY LOAN</Text>
</TouchableOpacity>
                   </View>
                   <View style={styles.box}>
                   <View style={{
                       width: 50,
                       height: 50,
                         alignItems: 'center',
                       justifyContent:'center',
                       
                       borderRadius: 50/2,
                       borderWidth: 0.5,
                       borderColor: '#facd05'
                   }}>
                   <Icon name="file-plus" size={30} color="#facd05" />
                     </View>
                   <Text style={{color:'#fff',fontWeight: 'normal'}}>Apply </Text>
                   <Text style={{color:'#fff',fontWeight: 'normal'}}>Loan </Text>
                   
                   </View>
                   <View style={styles.box}>
                   
                   <View style={{
                       width: 50,
                       height: 50,
                         alignItems: 'center',
                       justifyContent:'center',
                       
                       borderRadius: 50/2,
                       borderWidth: 0.5,
                       borderColor: '#61d2f3'
                   }}>
                   <Icon name="folder-clock-outline" size={30} color="#61d2f3" />
                     </View>
                   <Text style={{color:'#fff',fontWeight: 'normal'}}>Loan </Text>
                   <Text style={{color:'#fff',fontWeight: 'normal'}}>History </Text>
                   </View>
                   <View style={styles.box}>
                   <View style={{
                       width: 50,
                       height: 50,
                         alignItems: 'center',
                       justifyContent:'center',
                       
                       borderRadius: 50/2,
                       borderWidth: 0.5,
                       borderColor: "#ff0000"
                   }}>
                   <Icon name="account-outline" size={30} color="#ff0000"/>
                     </View>
                   <Text style={{color:'#fff',fontWeight: 'normal'}}>My </Text>
                   <Text style={{color:'#fff',fontWeight: 'normal'}}>Profile </Text>
                   
                   
                   </View>
                   <View style={styles.box}>
                   <View style={{
                       width: 50,
                       height: 50,
                         alignItems: 'center',
                       justifyContent:'center',
                       
                       borderRadius: 50/2,
                       borderWidth: 0.5,
                       borderColor: '#e005fa'
                   }}>
                   <Icon name="lock" size={30} color="#e005fa" />
                     </View>
                   <Text style={{color:'#fff',fontWeight: 'normal'}}>Change </Text>
                   <Text style={{color:'#fff',fontWeight: 'normal'}}>Password </Text>
                   </View>
                   <View style={styles.box}>
                   <View style={{
                       width: 50,
                       height: 50,
                         alignItems: 'center',
                       justifyContent:'center',
                       
                       borderRadius: 50/2,
                       borderWidth: 0.5,
                       borderColor: '#5ec067'
                   }}>
                   <Icon name="comment-processing-outline" size={30} color="#5ec067" />
                     </View>
                   <Text style={{color:'#fff',fontWeight: 'normal'}}>Chat</Text>
                   </View>
                   <View style={styles.box}>
                   <View style={{
                       width: 50,
                       height: 50,
                         alignItems: 'center',
                       justifyContent:'center',
                       
                       borderRadius: 50/2,
                       borderWidth: 0.5,
                       borderColor: '#b6b2b7'
                   }}>
                   <Icon name="dots-horizontal" size={30} color="#b6b2b7" />
                     </View>
                   <Text style={{color:'#fff',fontWeight: 'normal'}}>More</Text>
                   </View>
                    </View>
            
            </View>
        )

    }
}

const styles=StyleSheet.create({
        container:{
            flex:1,
            justifyContent: 'center',
            // alignItems: 'center',
            backgroundColor:'#282828'

        },
        buttonContainer:{
            backgroundColor: '#61d2f3',
            paddingVertical: 5,
            padding:30,
            borderWidth:0.5,
            borderRadius:5
            // flexGrow:0.4
           },
           buttonText:{
            color: '#fff',
            textAlign: 'center',
            fontWeight: '700'
           },
        text_value:{
            color:'#fff'
        },

        innercontainer:{
            flex:1,
            flexDirection: 'row',
            flexWrap: 'wrap',
            padding: 1,
            justifyContent:'center',
            alignItems: 'center',
            // position:'absolute',
            top:'45%',
            zIndex:2
        },
        box1:{
            
            width:Dimensions.get('window').width/2 -1,
            height:'17%',
            justifyContent:'center',
            alignItems: 'center',
            // borderRadius:2,
            backgroundColor:'#2f2929',
            borderWidth: 0.5,
            borderColor:'#adacac',
            // margin:0.5
        },
        box:{
            
            width:Dimensions.get('window').width/3 -1,
            height:'26%',
            justifyContent:'center',
            alignItems: 'center',
            backgroundColor:'#2f2929',
            // borderRadius:2,
            borderWidth: 0.5,
            borderColor:'#adacac',
            // margin:1
        },
        profile1: {
            width: 320,
            height: 320,
            backgroundColor: '#282828',
            borderRadius: 320/2,
            borderWidth: 0.5,
            borderColor: '#7f8c8d',
            position:'absolute',
            alignItems: 'center',
            justifyContent:'center',
            top:'2%',
            left:"5%",
            zIndex:1

        },
        profile2: {
            width: 260,
            height: 260,
            backgroundColor: '#282828',
              alignItems: 'center',
            justifyContent:'center',
            
            borderRadius: 260/2,
            borderWidth: 0.5,
            borderColor: '#7f8c8d'
        },
        profile3: {
            width: 200,
            height: 200,
            backgroundColor: '#282828',
            borderRadius: 200/2,
            borderWidth: 0.5,
            borderColor: '#7f8c8d',
            alignItems: 'center',
            justifyContent:'center'

        },
        profile4: {
            width: 140,
            height: 140,
            top:'17%',
            left:'30%',
            backgroundColor: '#ecf0f1',
            borderRadius: 70,
            borderWidth: 3,
            borderColor: '#2ecc71',
            alignItems: 'center',
            justifyContent:'center',
            zIndex:5,
            position:'absolute'

        },
        top:{
            height:'22%',
            justifyContent:'center',
            alignItems: 'center',
            backgroundColor:'#383838',
            // backgroundColor:'rgb(108, 122, 137)',
            zIndex:3,
            // position:'absolute',
            top:'28%'
        }

})