import React,{Component} from 'react'
import { View,Text,TextInput,Image,TouchableOpacity,StyleSheet}from 'react-native'

export default class Verify extends Component{
    static navigationOptions={
        header:null
    }

render(){
    return(
        <View style={styles.container}>
        
            <View style={styles.image}>
            <Image
                style={{flex:0.7,justifyContent:'flex-end', height: undefined, width: undefined}}      
                source={require('../images/Cashday2.png')}
                    resizeMode="contain"
                    />
                    <Text style={{color:'#fff',marginLeft:70,
                    alignItems:'center',fontSize:15,justifyContent:'flex-start'}}>Enter Verification Code</Text>
            </View>
            
            <View style={styles.verifycontainer}>
                <TextInput
                 returnKeyType="go" 
                 keyboardType='numeric'
                 underlineColorAndroid='#fff'
                 style={{width:200,color:'#fff',paddingTop:30}}
                />

                <View style={styles.verifybtns}>
                <View>
                <TouchableOpacity style={styles.buttonContainer} >      
                    <Text  style={styles.buttonText}>CANCEL</Text>
                </TouchableOpacity></View>
                <View>
                <TouchableOpacity  style={{backgroundColor:'#2980b6',paddingVertical: 5, padding:20}}>      
                    <Text  style={styles.buttonText}>VERIFY</Text>
                </TouchableOpacity>
                </View>
                </View>
            </View>


        </View>
    )
}



}

const styles=StyleSheet.create({

    container:{
        flex:1,
        backgroundColor:'#2c3e50',
        padding: 30,
    },
    verifybtns:{
        flex:1,
        paddingTop: 10,
        flexDirection: 'row',
        // alignItems:'center',
        justifyContent: 'space-between',
        
    },
    buttonContainer:{
        backgroundColor: '#95a5a6',
        paddingVertical: 5,
        paddingHorizontal: 5,
        padding:10,
        marginRight: 20
        // flexGrow:0.4
       },
    image:{
        justifyContent:'flex-end',
        flexGrow:0.5,
    },
    verifycontainer:{
        alignItems: 'center',
        height:10,
        justifyContent: 'flex-end',
        flex:0.5
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
       }

})