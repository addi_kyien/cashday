/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import SplashScreen from 'react-native-smart-splash-screen'
import  Verify from './screens/verify';
import  Login from './screens/login';
import  Register from './screens/register';
import Home from './home'

const Navigator=createStackNavigator({
  // splash:{
  //   screen:Splash
  // },
  login:{
    screen:Login
  },
  register:{
    screen:Register
  },
  verify:{
    screen:Verify
  },
  home:{
    screen:Home
  }
})
export default class App extends Component {

  componentDidMount() {
    SplashScreen.close({
      animationType: SplashScreen.animationType.scale,
      duration: 1000,
      delay: 500,
   })
  }

render(){ 
return(
<Navigator/>
)
}

}