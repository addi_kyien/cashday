import React,{Component } from "react"
import Dash from './screens/dash'
import Verify from './screens/verify'
import {View,Text,Dimensions}from 'react-native'
import { createDrawerNavigator,DrawerItems} from "react-navigation";
import SideMenu from './sidemenu'



const Router=createDrawerNavigator({
    Home: {
        screen: Dash,
      },
      Notifications: {
        screen: Verify,
    }
},
    {
          contentComponent: SideMenu,
          
          // contentComponent: (props) => (
          //   <View style={{backgroundColor:'#2c3e50'}}>
          //     {/* <Text>Custom Header</Text>
          //     <DrawerItems {...props} />
          //     <Text>Custom Footer</Text> */}
          //   </View>
          // ),
          drawerWidth:Dimensions.get('window').width -100,
          drawerBackgroundColor:'#2c3e50'
        }
    
)

export default Router