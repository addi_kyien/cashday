import React,{ Component } from "react";
import {  View,Text,StyleSheet } from 'react-native'
import { Select, Option } from 'react-native-select-lists'
import { Collapse, CollapseHeader, CollapseBody } from "accordion-collapse-react-native";
import { Thumbnail, List, ListItem, Separator } from 'native-base';
import Triangle from 'react-native-triangle';


export default class SideMenu extends Component{

  render(){
    return(

  <View>
      <ListItem last>
          <Text style={styles.nav_text}>Change Pin</Text>
        </ListItem>
        <ListItem last>
          <Text style={styles.nav_text}>FAQs</Text>
        </ListItem>
        <ListItem last>
          <Text style={styles.nav_text}>Terms and Conditions</Text>
        </ListItem>
        <ListItem last>
          <Text style={styles.nav_text}>About Us</Text>
        </ListItem>
        <ListItem last>
        <View style={styles.dropdown}>
             <Collapse>
             <CollapseHeader>
        
        <Text style={styles.nav_text_collapse}>Talk to Us</Text>
        <View style={styles.dropdown_triangle}>
        <Triangle
  width={12}
  height={7}
  color={'#fff'}
  direction={'down'}
/></View>
      </CollapseHeader>
      
      <CollapseBody >
        <ListItem >
          <Text style={styles.nav_text}>Tell a friend</Text>
        </ListItem>
        <ListItem>
          <Text style={styles.nav_text}>Rate us</Text>
        </ListItem>
        <ListItem >
          <Text style={styles.nav_text}>Call Us </Text>
        </ListItem>
        <ListItem >
          <Text style={styles.nav_text}>E-mail </Text>
        </ListItem>
        <ListItem >
          <Text style={styles.nav_text}>facebook </Text>
        </ListItem>
        <ListItem >
          <Text style={styles.nav_text}>Twitter </Text>
        </ListItem>
      </CollapseBody>
    </Collapse> 
</View>
        </ListItem>
        <ListItem last>
          <Text style={styles.nav_text}>Logout</Text>
        </ListItem>
    
   
  </View>
   
  
    )
}
}

const styles=StyleSheet.create({

  nav_text:{
    color:'#fff',
    fontWeight: 'normal',
  },
  nav_text_collapse:{
    color:'#fff',
    fontWeight: 'normal',
    alignItems:'center',
    // paddingLeft: '2%',

  },
  container_view:{
    justifyContent:'center',
    alignItems: 'center',

  },
  dropdown:{
    alignItems: 'stretch',
    flex:1
    // position:'absolute',
    // left:'90%',
    // top:'20%'
  },
  dropdown_triangle:{
    alignItems: 'center',
    position:'absolute',
    left:'80%',
    top:'21%'
  },
  cll:{
    width:'100%',

  }
})