import * as firebase from 'firebase';
import firestore from 'firebase/firestore'

const settings = {timestampsInSnapshots: true};

const config = {

    apiKey: "AIzaSyAGp6dx8Kxqc2Nix1Ya3J0V4OqT8L863JA",
    authDomain: "cashdaynew.firebaseapp.com",
    databaseURL: "https://cashdaynew.firebaseio.com",
    projectId: "cashdaynew",
    storageBucket: "cashdaynew.appspot.com",
    messagingSenderId: "731643357137"

};
firebase.initializeApp(config);

firebase.firestore().settings(settings);

export default firebase;